#!/bin/bash

set -ex

for b in gpsd gpscat gpspipe cgps gpsfake ubxtool; do
	$b --help |& grep -i ^usage:
done

exit 0
